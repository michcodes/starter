const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  mode: 'jit', 
  purge: ['./src/_includes/**/*.njk', './src/*.html'],
  darkMode: false,
  theme: {
    extend: {
      fontFamily: {
        sans: ['Inter', ...defaultTheme.fontFamily.sans],
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
